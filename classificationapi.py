import requests
import pandas as pd
from flask import Flask, request
from flask_restful import Resource, Api
import json
import logging
from classificationmodel import load_model

app = Flask(__name__)
api = Api(app)


# app.config.from_object('settings')


@app.before_first_request
def setup_logging():
    if not app.debug:
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)

        app.logger.addHandler(handler)
        app.logger.setLevel(logging.INFO)


def get_json_response(url):
    app.logger.debug(url)
    response = json.loads(requests.get(url).text)
    return response


class Classify(Resource):
    def get(self):
        result = {}

        args = request.args
        question = args['q']

        data = pd.DataFrame([{'feature': question}])['feature']

        category_model = load_model("category")
        category = category_model.predict_data(data)

        app.logger.info(category)
        if category[0] < 0.8:
            result['label'] = {'main': 'unknown', 'sub': 'unknown'}
        elif category[1] == 'greeting':
            result['label'] = {'main': category[1], 'sub': 'unknown'}
        else:
            weather_model = load_model("weather")
            weather = weather_model.predict_data(data)

            result['label'] = {'main': category[1], 'sub': weather[1]}

        result['proba'] = category[0]

        return result


api.add_resource(Classify, '/classify')

if __name__ == '__main__':
    app.run()
