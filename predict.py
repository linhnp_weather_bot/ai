from classificationmodel import *
import pandas as pd


def strtodataframe(str):
    return pd.DataFrame([{'feature': str}])


if __name__ == '__main__':
    text = ""

    while text != 'quit':
        text = input()
        df = strtodataframe(text)

        category_model = load_model("category")
        category = category_model.predict_data(df['feature'])

        print(category)
        # TODO: multiple value
        if category[1] == 'weather':
            weather_model = load_model("weather")
            time = weather_model.predict_data(df['feature'])
            print(time)
