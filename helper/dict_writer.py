from os.path import dirname, join
import io


def read(filename):
    with io.open(filename, 'r', encoding='utf-8') as f:
        text = f.read()

    return text


def write(filename, text):
    with io.open(filename, 'w', encoding='utf-8') as f:
        f.write(text)


def update_dict():
    stopwords_dict = read(join(join(dirname(dirname(__name__)), 'data'), 'vietnamese-stopwords.txt')).split("\n")
    append_list = read(join(join(dirname(dirname(__name__)), 'helper'), 'append_word.txt')).split("\n")
    final_list = sorted(set([word.lower() for word in stopwords_dict + append_list if word]))
    normal = '\n'.join(final_list)
    dash = '\n'.join(word.replace(' ', '_') for word in final_list)

    write(join(join(dirname(dirname(__name__)), 'data'), 'vietnamese-stopwords.txt'), normal)
    write(join(join(dirname(dirname(__name__)), 'data'), 'vietnamese-stopwords-dash.txt'), dash)


if __name__ == '__main__':
    update_dict()
