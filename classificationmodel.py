from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.externals import joblib
from common import words2features
import logging

# TODO: replace pyvi

format = '%(asctime)s %(levelname)s %(message)s'
logging.basicConfig(format=format, level=logging.INFO)
logger = logging.getLogger(__name__)


class Preprocessor(BaseEstimator, TransformerMixin):

    def __init__(self):
        self.tokenizer = words2features
        pass

    def fit(self, *_):
        return self

    """
    raw_document: pandas.DataFrame in this case
    """
    def transform(self, raw_document):
        return raw_document.apply(lambda text: self.tokenizer(text))


class NaiveBayesModel:

    def __init__(self, model, model_name):
        self.model = model
        self.name = model_name

    def train(self, data, target):
        logger.info("Training model")
        self.model.fit(data, target)

    def save(self):
        joblib.dump(self, 'trained_models/' + self.name + '_model.pkl')
        logger.info("Save done")

    def predict_data(self, data):
        predict = zip(self.model.predict_proba(data)[0], self.model.classes_)
        return sorted(predict, reverse=True)[0]


def get_pipeline():
    return Pipeline([
        ('preprocessor', Preprocessor()),  # processing input
        ('vect', CountVectorizer()),  # bag-of-words
        ('tfidf', TfidfTransformer()),  # tf-idf
        ('clf', MultinomialNB()),  # model naive bayes
    ])


def load_model(name):
    return joblib.load('trained_models/' + name + '_model.pkl')
