from os.path import dirname, join
from common.file_io import read
from common.singleton import Singleton


@Singleton
class StopWordsLoader:
    def __init__(self):
        self.path = join(join(dirname(dirname(__file__)), 'data'), 'vietnamese-stopwords-dash.txt')
        self._word_list = None

    @property
    def words(self):
        if not self._word_list:
            self._word_list = read(self.path).split('\n')

        return self._word_list
