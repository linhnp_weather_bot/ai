import re
import unicodedata
from common.stopwords_loader import StopWordsLoader
from underthesea import word_sent
import logging

# string.punctuation minus underscore
PUNCTUATION = r"""!"#$%&'()*+,-./:;<=>?@[\]^`{|}~"""

format = '%(asctime)s %(levelname)s %(message)s'
logging.basicConfig(format=format, level=logging.INFO)
logger = logging.getLogger(__name__)


def convert2unsign(text):
    text = re.sub(u'Đ', 'D', text)
    text = re.sub(u'đ', 'd', text)

    return ''.join(c for c in unicodedata.normalize("NFD", text) if unicodedata.category(c) != 'Mn')


def remove_punctuation(text):
    # https://stackoverflow.com/questions/34293875/how-to-remove-punctuation-marks-from-a-string-in-python-3-x-using-translate
    translator = str.maketrans('', '', PUNCTUATION)
    return text.translate(translator)


def words2features(text):
    text = remove_punctuation(text.lower())
    text = ' '.join(
        word for word in word_sent(text, format='text').split() if
        word not in StopWordsLoader.Instance().words)
    text = convert2unsign(text)
    logger.info(text)

    return text
