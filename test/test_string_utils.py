import unittest
from common import util_string


class TestStringUtilsModule(unittest.TestCase):

    def test_remove_functuation(self):
        self.assertEqual(util_string.remove_punctuation('a.a.a.a'), 'aaaa')
        self.assertEqual(util_string.remove_punctuation('aaaa'), 'aaaa')
        self.assertEqual(util_string.remove_punctuation('.,/?;:><"{}][|+_)(*&^%$#@!~`-"\''), '_')

    def test_lowercase(self):
        self.assertEqual('AnnA'.lower(), 'anna')


if __name__ == "__main__":
    unittest.main()
