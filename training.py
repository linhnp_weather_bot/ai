from classificationmodel import NaiveBayesModel, get_pipeline
import pandas as pd
import logging

format = '%(asctime)s %(levelname)s %(message)s'
logging.basicConfig(format=format, level=logging.INFO)
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    df_train = pd.read_csv('training_input.csv', encoding='utf-8')

    logger.info('Training categor')

    category_model = NaiveBayesModel(get_pipeline(), "category")
    category_model.train(df_train['text'], df_train['category'])
    category_model.save()

    logger.info('Traning category done')

    df_weather_train = df_train[df_train.category == 'weather']

    logger.info('Training weather')
    weather_model = NaiveBayesModel(get_pipeline(), "weather")
    weather_model.train(df_weather_train['text'], df_weather_train['sub'])
    weather_model.save()
    logger.info('Training weather done')
